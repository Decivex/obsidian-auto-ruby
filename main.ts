import { App, ButtonComponent, Plugin, PluginSettingTab, Setting, TextComponent } from 'obsidian';
import { Decoration, DecorationSet, EditorView, PluginSpec, PluginValue, ViewPlugin, ViewUpdate, WidgetType } from "@codemirror/view";
import { RangeSetBuilder } from "@codemirror/state";

interface RubyEntry {
	text: string;
	annotation: string;
}

interface EntryList {
	name: string;
	list: RubyEntry[];
}

interface AutoRubyPluginSettings {
	entryList: EntryList[];
}

const DEFAULT_SETTINGS: AutoRubyPluginSettings = {
	entryList: []
}

class RubyWidget extends WidgetType {
	constructor(private text: string, private annotation: string) {
		super();
	}

	toDOM(view: EditorView): HTMLElement {
		const ruby = document.createElement('ruby');
		ruby.setText(this.text);
		ruby.createEl('rt').setText(this.annotation);
		return ruby;
	}
}

function compareEntries(a: RubyEntry, b: RubyEntry) {
	return (b.text.length - a.text.length) || (a.text.localeCompare(b.text)) || (a.annotation.localeCompare(b.annotation));
}

class TextFinder {
	entries: RubyEntry[] = []

	setEntries(entryLists: EntryList[]) {
		this.entries = entryLists.flatMap(entryList => entryList.list)
			.filter(entry => entry.text.length > 0)
			.sort(compareEntries);
	}

	findInText(text: string) {
		let offset = 0;
		const results: { from: number, to: number, text: string, annotation: string }[] = [];

		while (offset < text.length) {
			// TODO: Create index for entries
			for (const entry of this.entries) {
				if (text.substring(offset).startsWith(entry.text)) {
					results.push({
						from: offset,
						to: offset+entry.text.length,
						...entry
					});
					offset += entry.text.length - 1;
					break;
				}
			}
			offset++;
		}

		return results;
	}
}

interface Range {
	from: number;
	to: number;
}

function* subtractRanges(a: readonly Range[], b: readonly Range[]): Generator<Range> {
	const BLOCK = 0;
	const UNBLOCK = 1;
	const START = 2;
	const STOP = 3;
	type Event = [number, 0|1|2|3];

	const events: Event[] = [];
	a.forEach(({ from, to }) => events.push([from, START], [to, STOP]));
	b.forEach(({ from, to }) => events.push([from, BLOCK], [to, UNBLOCK]));
	events.sort((a, b) => a[0] - b[0] || a[1] - b[1]);

	let blocked = false;
	let currentRangeStart = null;

	for (const [at, ev] of events) {
		switch (ev) {
			case UNBLOCK:
				blocked = false;
				if (currentRangeStart != null) {
					currentRangeStart = at;
				}
				break;
			case BLOCK:
				if (currentRangeStart != null) {
					yield ({from: currentRangeStart, to: at});
				}
				blocked = true;
				break;
			case START:
				currentRangeStart = at;
				break;
			case STOP:
				if (!blocked && currentRangeStart != null) {
					yield ({from: currentRangeStart, to: at});
				}
				currentRangeStart = null;
				break;

		}
	}
}

const pluginSpec: PluginSpec<AutoRubyEditorExtension> = {
	decorations: (value: AutoRubyEditorExtension) => value.decorations,
};

class AutoRubyEditorExtension implements PluginValue {
	decorations: DecorationSet;

	constructor(view: EditorView, private textFinder: TextFinder) {
		this.decorations = this.buildDecorations(view);
	}

	update(update: ViewUpdate) {
		if (update.docChanged || update.viewportChanged || update.selectionSet) {
			this.decorations = this.buildDecorations(update.view);
		}
	}

	destroy() {}

	private buildDecorations(view: EditorView) {
		const builder = new RangeSetBuilder<Decoration>();

		const ranges = [...subtractRanges(view.visibleRanges, view.state.selection.ranges)];

		for (const { from, to } of ranges) {
			const text = view.state.sliceDoc(from, to);
			const results = this.textFinder.findInText(text);
			results.forEach(r => {
				builder.add(from + r.from, from + r.to, Decoration.replace({
					widget: new RubyWidget(r.text, r.annotation),
				}));
			});
		}

		return builder.finish();
	}
}

export default class AutoRubyPlugin extends Plugin {
	settings: AutoRubyPluginSettings;
	textFinder = new TextFinder();

	async onload() {
		await this.loadSettings();

		// This adds a settings tab so the user can configure various aspects of the plugin
		this.addSettingTab(new AutoRubySettingTab(this.app, this));

		const extension = ViewPlugin.define(
			(view) => new AutoRubyEditorExtension(view, this.textFinder), pluginSpec
		);
		this.registerEditorExtension(extension);
	}

	onunload() {

	}

	async loadSettings() {
		this.settings = Object.assign({}, DEFAULT_SETTINGS, await this.loadData());
		this.textFinder.setEntries(this.settings.entryList);
	}

	async saveSettings() {
		this.textFinder.setEntries(this.settings.entryList);
		await this.saveData(this.settings);
	}
}

class EntryTableComponent {
	private table: HTMLTableSectionElement;

	constructor(
		private parentEl: HTMLElement,
		private plugin: AutoRubyPlugin,
		private entries: RubyEntry[]
	) {
		const table = parentEl.createEl('table');

		const colgroup = table.createEl('colgroup');
		colgroup.createEl('col', { cls: 'max' });
		colgroup.createEl('col', { cls: 'max' });
		colgroup.createEl('col', { cls: 'min' });

		const header = table.createTHead().insertRow();
		header.insertCell().setText('Text');
		header.insertCell().setText('Annotation');

		new ButtonComponent(header.insertCell())
			.setIcon('plus')
			.onClick(async evt => {
				const entry = { annotation: "", text: "" };
				this.entries.push(entry);
				this.addEntry(entry);
				await this.plugin.saveSettings();
			});

		this.table = table.createTBody();
		entries.forEach(this.addEntry.bind(this));
	}

	addEntry(entry: RubyEntry) {
		const row = this.table.insertRow();
		new TextComponent(row.insertCell())
			.setValue(entry.text)
			.setPlaceholder('Text')
			.onChange(async value => {
				entry.text = value;
				await this.saveData();
			});

		new TextComponent(row.insertCell())
			.setValue(entry.annotation)
			.setPlaceholder('Annotation')
			.onChange(async value => {
				entry.annotation = value;
				await this.saveData();
			});

		new ButtonComponent(row.insertCell())
			.setIcon('trash')
			.onClick(async evt => {
				this.entries.remove(entry);
				row.remove();
				await this.saveData();
			});
	}

	saveData() {
		return this.plugin.saveSettings();
	}
}

class AutoRubySettingTab extends PluginSettingTab {
	plugin: AutoRubyPlugin;

	constructor(app: App, plugin: AutoRubyPlugin) {
		super(app, plugin);
		this.plugin = plugin;
	}

	display(): void {
		const {containerEl} = this;

		containerEl.empty();

		containerEl.createEl('h1', {text: 'Lists'});

		this.plugin.settings.entryList.forEach(entryList => this.addList(containerEl, entryList));

		new Setting(containerEl)
			.addButton(button => button
				.setButtonText('Add entry list')
				.onClick(evt => {
					const entryList = {
						name: 'Untitled List',
						list: []
					};
					this.plugin.settings.entryList.push(entryList);
					this.addList(containerEl, entryList);
				})
			);

	}

	addList(parentEl: HTMLElement, entryList: EntryList) {
		const listPane = parentEl.createDiv({
			cls: 'entry-list-pane'
		});

		const title = listPane.createEl('h2', { text: entryList.name });

		new ButtonComponent(title)
			.setButtonText('Delete')
			.setWarning()
			.onClick(async evt => {
				this.plugin.settings.entryList.remove(entryList);
				listPane.remove();
				await this.plugin.saveSettings();
			});

		new Setting(listPane)
			.setName('Entry list name')
			.addText(text => text
				.setValue(entryList.name)
				.onChange(async value => {
					entryList.name = value;
					title.setText(value);
					await this.plugin.saveSettings();
				}));


		listPane.createEl('h3', {text: 'Entries'});

		new EntryTableComponent(listPane, this.plugin, entryList.list);
	}
}
