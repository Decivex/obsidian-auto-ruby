# Auto Ruby Obsidian Plugin

Automatically add `<ruby>` tags to your vault using a configurable word list.

## Todo:

- [ ] Add an index to speed up finding matches in notes.
- [ ] Make entry lists collapsible.
- [ ] Allow disabling entry lists.
- [ ] Add search function for entries.
- [ ] Allow reordering of entries.
- [ ] Allow sorting of entries.
